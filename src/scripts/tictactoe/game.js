import React from "react";
import Board from './board'
import winconditions from './winconditions'
import {Button, Clearfix} from 'react-bootstrap';

import StepRangeSlider from 'react-step-range-slider';
const range = [
    {value: 3, step: 1}, // acts as min value
    {value: 9} // acts as max value
]

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sliderValue: 3,
            squaresPerSide: 3,
            winconditions: winconditions(3),
            winners: Array(3).fill(null),
            history: [{
                squares: Array(9).fill(null),
            }],
            stepNumber: 0,
            xIsNext: true,
        };
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (this.chickenDinner(squares) || squares[i]) {
            return;
        }
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([
                {squares: squares,}
            ]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext,
        });
    }

    jumpTo(step) {
        this.setState({
            winners: Array(3).fill(null),
            stepNumber: step,
            xIsNext: (step % 2) === 0,
        });
    }

    resizeBoard(){
        let numcol = this.state.sliderValue;
        this.setState({
            squaresPerSide: numcol,
            history: [{
                squares: Array(Math.pow(3, 2)).fill(null),
            }],
            stepNumber: 0,
            xIsNext: true,
            winconditions: winconditions(numcol),
        })
    }

    slide(number){
        this.setState({sliderValue: number})
    }

    sliderOnCurrentActiveValue(){
        let current = this.state.squaresPerSide;
        let slider = this.state.sliderValue;
        return current === slider;
    }

    chickenDinner(squares){
        var lines = this.state.winconditions;
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                return {
                    player: squares[a],
                    winningSquares: [a, b, c],
                }
            }
        }
        return null;
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = this.chickenDinner(current.squares);

        const moves = history.map((step, move) => {
            const desc = move ?
                'Go to move #' + move :
                'Go to game start';
            return (
                <li key={move}>
                    <Button bsStyle='info' onClick={() => this.jumpTo(move)}>{desc}</Button>
                </li>
            );
        });

        let status;
        let gridSizeInfo = ' Current board: ' + Math.pow(this.state.squaresPerSide, 2) + ' squares.';
        if (winner) {
            status = 'Winner: ' + winner.player;
        } else {
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }
        let buttonText = 'Load ' + Math.pow(this.state.sliderValue, 2) + ' square grid.';

        return (
            <div className='gameContent'>
                <h1>
                    ReacTicTacToe
                </h1>
                <h2>
                    <a href={'https://reactjs.org/tutorial/tutorial.html'}>
                        Built on top of React intro tutorial
                    </a>
                </h2>
                <div className="sliderDiv">
                    <StepRangeSlider id="slider"
                                     value={3}
                                     range={range}
                                     onChange={value => this.slide(value)}
                    />
                    <Button bsStyle='primary' className='gridSizeButton'
                            onClick={() => this.resizeBoard()} disabled={this.sliderOnCurrentActiveValue()}>{buttonText}</Button>
                </div>
                <Clearfix/>
                <div className="game">
                    <div className="game-info">
                        <div>{gridSizeInfo}</div>
                        <div>{status}</div>
                        <div>History:</div>
                        <ol>{moves}</ol>
                    </div>
                    <div className="game-board">
                        <Board
                            colNumber={this.state.squaresPerSide}
                            winners = {winner}
                            squares={current.squares}
                            onClick={(i) => this.handleClick(i)}
                        />

                    </div>
                </div>
            </div>
        );
    }
}

export default Game;