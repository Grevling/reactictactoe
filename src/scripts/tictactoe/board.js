import React from "react";
import Square from './square'

class Board extends React.Component {
    renderSquare(i) {
        let winners = this.props.winners;
        let winnerBool = (winners && winners.winningSquares.includes(i))
        return (
            <Square
                winner = {winnerBool}
                value={this.props.squares[i]}
                onClick={() => this.props.onClick(i)}
            />
        );
    }

    getRow(numCols, curNum) {
        var squares = [];
        var thingy = curNum;
        for (var i = 0; i < numCols; i++) {
            squares[i] = this.renderSquare(thingy+i);
        }
        return (
            <div className="board-row">
                {squares.map(square =>{return square;})}
            </div>
        );
    }

    render() {
        let colNumber = this.props.colNumber;
        var rows = [];
        var curNum = 0;
        for (var i = 0; i < colNumber; i++) {
            rows[i] = this.getRow(colNumber, curNum);
            curNum += colNumber;
            // debugger;
        }
        return (
            <div>
                {rows.map(row =>{return row;})}
            </div>
        );
    }


}

export default Board;