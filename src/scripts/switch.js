import React from "react";
import {Button} from 'react-bootstrap';
import Pattern from "./patterns/pattern";
import Game from './tictactoe/game'
import html2canvas from 'html2canvas';
// import winconditions from "./tictactoe/winconditions";



class Switch extends React.Component {
    constructor(){
        super();

        this.state = {
            mode: "menu",
        };
        // this.gameMode = true;
        console.log('constructor');
}

    getGame(){
        /*<Game deyta={setDeytah} thingy=this.state />*/
        return (
            <Game/>
        );
    }

    getPattern(){
        return (
            <Pattern/>
        );
    }

    getTest(){
        return (
            <Pattern/>
        );
    }

    getMenu(){
        return (
            <div className='content'>
                <h1>RETURNTOTHEPLANETOFHANDS</h1>
                <div id="menuDiv" className="capture" ref="Progress1">
                    <div id="patternDiv" className="menuItem" onClick={() => this.setMode("pattern")}></div>
                    <div id="gameDiv" className="menuItem" onClick={() => this.setMode("game")}></div>
                    <div id="testDiv" className="menuItem" onClick={() => this.setMode("test")}></div>
                </div>
            </div>
        );
    }

    setMode(mode){
        this.setState({
            mode: mode
        })
    }

    getMenuButton(argBool){
        if(argBool) {
            return (
                <Button bsStyle='primary' className='modeButton' onClick={() => this.setMode("menu")}>{"Back To Menu"}</Button>
            );
        }
    }


    render() {
        console.log('rendering in switch');
        var mode = this.state.mode;
        let renderButton = !(mode == "menu");

        var thingy = this.getMenu();
        if(mode == "game") { thingy = this.getGame(); }
        else if(mode == "pattern") { thingy = this.getPattern(); }
        else if(mode == "test") { thingy = this.getTest(); }

        return (
            <div className='content'>
                {/*<Button bsStyle='primary' className='modeButton' onClick={() => this.switchMode()}>{buttonText}</Button>*/}
                {this.getMenuButton(renderButton)}
                {thingy}
            </div>
        );

    }

}


export default Switch;