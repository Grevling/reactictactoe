import React from "react";
import {Button, Clearfix} from 'react-bootstrap';
import StepRangeSlider from 'react-step-range-slider';
import html2canvas from "html2canvas";


const sideLength = 500;
const startRes = 5;


const range = [
    {value: 3, step: 2}, // acts as min value
    {value: 65} // acts as max value
];


//   http://html2canvas.hertzen.com/


class Pattern extends React.Component {
    constructor(){
        super();
        let numberOfTilesRoot = startRes;
        this.state = {
            painted: false,
            sliderValue: numberOfTilesRoot,
            sideLength: sideLength,
            numberOfTilesRoot: numberOfTilesRoot,
            tileDim: sideLength/numberOfTilesRoot,
            shades: 7,
            classes: this.getBgcClasses(numberOfTilesRoot),
            mirrorHorizontal: true,
            mirrorVertical: true,
            mirrorDiagonal: true,
        }
    }

    refresh(){
        this.deletePicture();
        let numberOfTilesRootVar = this.state.numberOfTilesRoot;
        this.setState({
            classes: this.getBgcClasses(numberOfTilesRootVar)
        });
    }

    changeResolution(){
        this.deletePicture();
        let numberOfTilesRootVar = this.state.sliderValue;
        let tileDim = sideLength/numberOfTilesRootVar;
        let newTileDim = Math.round(tileDim);
        let newSideLength = newTileDim*numberOfTilesRootVar;

        if (newSideLength > sideLength) {
            newTileDim = Math.floor(tileDim);
            newSideLength = newTileDim*numberOfTilesRootVar;
        }

        this.setState({
            numberOfTilesRoot: numberOfTilesRootVar,
            tileDim: newTileDim,
            sideLength: newSideLength,
            classes: this.getBgcClasses(numberOfTilesRootVar)
        })
    }

    slide(number){
        this.setState({sliderValue: number})
        // this.changeResolution() //enable to change resolution by sliding, disable to require resolution button
    }

    sliderOnCurrentActiveValue(){
        let current = this.state.numberOfTilesRoot;
        let slider = this.state.sliderValue;
        return current === slider;
    }

    getBgcClasses(numberOfTilesRoot){
        var bgcClasses = [];
        for (var i = 0; i < numberOfTilesRoot; i++) {
            bgcClasses[i] = [];
            for (var j = 0; j < numberOfTilesRoot; j++) {
                bgcClasses[i][j] = getRandomInt(0,7);
            }
        }
        return bgcClasses;
    }


    getTiles(){
        var t0 = performance.now();
        var tiles = [];
        let counter = 0;
        var tileDim = this.state.tileDim;
        var classIds = this.state.classes;


        if(this.state.mirrorDiagonal) {
            classIds = this.mirrorGridDiag(classIds);
        }
        if(this.state.mirrorHorizontal) {
            classIds = this.mirrorGridHori(classIds);
        }
        if(this.state.mirrorVertical) {
            classIds = this.mirrorGridVerti(classIds);
        }

        classIds.forEach(function (classIdList){
            classIdList.forEach(function (classId) {
                let style = {
                    height: tileDim,
                    width: tileDim
                };
                let classString = 'tile bgc' + classId;
                tiles[counter] = <div key={'tile'+counter} className={classString} style={style}/>;
                counter ++;
            });
        });

        var t1 = performance.now();
        console.log("Call to getTiles took " + (t1 - t0) + " milliseconds.");
        return tiles;
    }

    mirrorGridHori(grid){
        var numberOfTilesRootVar = this.state.numberOfTilesRoot;
        var half = numberOfTilesRootVar/2;
        var returnArray = [];
        for (var i = 0; i < numberOfTilesRootVar; i++) {
            returnArray[i] = [];
            for (var j = 0; j < numberOfTilesRootVar; j++) {
                let value = grid[i][j];
                if(j > half) {
                    let thingy = j - half;
                    let k = half - thingy - 1;
                    value = grid[i][k];
                }
                returnArray[i][j] = value;
            }
        }

        return returnArray;
    }

    mirrorGridVerti(grid){
        var numberOfTilesRootVar = this.state.numberOfTilesRoot;
        var half = numberOfTilesRootVar/2;
        var returnArray = [];
        for (var i = 0; i < numberOfTilesRootVar; i++) {
            returnArray[i] = [];
            for (var j = 0; j < numberOfTilesRootVar; j++) {
                let value = grid[i][j];
                if(i > half) {
                    let thingy = i - half;
                    let h = half - thingy - 1;
                    value = grid[h][j];
                }
                returnArray[i][j] = value;
            }
        }

        return returnArray;
    }

    mirrorGridDiag(grid){
        var numberOfTilesRootVar = this.state.numberOfTilesRoot;
        var returnArray = [];
        for (var i = 0; i < numberOfTilesRootVar; i++) {
            returnArray[i] = [];
            for (var j = 0; j < numberOfTilesRootVar; j++) {
                let value = grid[i][j];
                if(i > j) {
                    value = grid[j][i];
                }
                returnArray[i][j] = value;
            }
        }
        return returnArray;
    }


    toggleMirrorHori(){
        let wasMirroring = this.state.mirrorHorizontal;
        this.setState({
            mirrorHorizontal: !wasMirroring
        });
    }

    toggleMirrorVerti(){
        let wasMirroring = this.state.mirrorVertical;
        this.setState({
            mirrorVertical: !wasMirroring
        });
    }

    toggleMirrorDiag(){
        let wasMirroring = this.state.mirrorDiagonal;
        this.setState({
            mirrorDiagonal: !wasMirroring
        });
    }

    getButtonType(on){
        if(on) {
            return 'success';
        }else {
            return 'danger';
        }
    }

    paint(){

        if(this.state.painted){
            this.deletePicture();
        }

        this.setPainted(true);

        let gridVar = document.getElementById("one");
        let canVar = document.getElementById("two");
        let oldPic = canVar.children.item(0);
        if(oldPic != null) {
            canVar.removeChild(oldPic);
        }

        let options = {
            backgroundColor: "red",
            scale : 1,
        };

        html2canvas(gridVar, options).then(function(canvas) {
            canVar.appendChild(canvas);
            document.getElementById("one").style.display = "none";
            document.getElementById("two").style.display = "block";
        });
    }

    deletePicture(){
        this.setPainted(false);
        let two = document.getElementById("two");
        let oldPic = two.children.item(0);
        if(oldPic != null) {
            two.removeChild(oldPic);
        }
        document.getElementById("one").style.display = "block";
        document.getElementById("two").style.display = "none";
    }

    setPainted(painted){
        this.setState({
            painted: painted
        });
    }

    render() {
        let sideLength = this.state.sideLength;
        const gridStyle = {
            width: sideLength,
            height: sideLength};

        const twoStyle = {
            width: sideLength,
            height: sideLength,
            display: 'none'};

        return (
            <div className='patternContent'>
                <h1>
                    Random Patterns?
                </h1>
                <h2>
                    <a href={'https://soundcloud.com/naden'}>
                        Patterns like Naden.
                    </a>
                </h2>
                <div className="sliderDiv">
                    <StepRangeSlider id="slider"
                                     value={startRes}
                                     range={range}
                                     onChange={value => this.slide(value)}
                    />
                    <Button bsStyle='primary' className='gridSizeButton' //todo reclassify buttons
                            onClick={() => this.changeResolution()} disabled={this.sliderOnCurrentActiveValue()}>{'Change Resolution'}</Button>
                    <Button bsStyle='primary' className='gridSizeButton'
                            onClick={() => this.refresh()}>{'Refresh'}</Button>
                    <Button bsStyle="info" className='paintButton'
                            onClick={() => this.paint(sideLength)}>{'Convert to Picture'}</Button>
                    <Clearfix/>
                </div>

                <div id='mirrorButtonDiv'>
                    <Button bsStyle={this.getButtonType(this.state.mirrorHorizontal)} className='horizontalMirrorButton'
                            onClick={() => this.toggleMirrorHori()}>{'Mirror |'}</Button>
                    <Button bsStyle={this.getButtonType(this.state.mirrorDiagonal)} className='diagonalMirrorButton'
                            onClick={() => this.toggleMirrorDiag()}>{'Mirror \\'}</Button>
                    <Button bsStyle={this.getButtonType(this.state.mirrorVertical)} className='verticalMirrorButton'
                            onClick={() => this.toggleMirrorVerti()}>{'Mirror -'}</Button>
                </div>
                <div id={'one'}
                     style={gridStyle}
                     onClick={() => this.refresh()}
                >
                    {this.getTiles()}
                </div>

                {/*{time1 = performance.now()}*/}
                {/*{console.log("Renderthing took " + (time1 - time0) + " milliseconds.")}*/}
                <Clearfix/>
                {/*<div id="canvas"/>*/}
                <div id="two" style={twoStyle}/>
            </div>
        );
    }
}

// width: this.state.sideLength,
// height:this.state.sideLength
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
export default Pattern
